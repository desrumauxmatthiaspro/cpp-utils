# Utils

The utils project contains some function & class to optimize some part of a project. The project has been start since august 2019.

## Parser

This librairy the following parser :

- .properties

## Ressources

This librairy optimize the ressource loading and embded static ressources to a quicker loading.

## Authors

| Authors | Contact | Since | OS |
| :- | :- | :-: | :-: |
| Desrumaux Matthias | desrumaux.matthias.pro@gmail.com | August 2019 | :checkered_flag: :penguin: |
