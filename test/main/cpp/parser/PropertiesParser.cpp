#include <gtest/gtest.h>
#include <sstream>

#include "parser/PropertiesParser.hpp"

using namespace Utils::Parser;

TEST( properties_parser, on_a_single_value ){

    PropertiesParser parser;
    std::istringstream iss( "test=work" );

    Properties p = parser.parse( iss );

    ASSERT_EQ( p.size(), 1 );
    ASSERT_EQ( p[ "test" ], "work" );
}

TEST( properties_parser, on_multiple_value ){

    PropertiesParser parser;

    std::string mock =  "test=work\n"
                        "other=\n"
                        "key=value\n";
    std::istringstream iss( mock );

    Properties p = parser.parse( iss );

    ASSERT_EQ( p.size(), 3 );
    ASSERT_EQ( p[ "test" ], "work" );
    ASSERT_EQ( p[ "other" ], "" );
    ASSERT_EQ( p[ "key" ], "value" );
}

TEST( properties_parser, a_token_can_t_be_empty ){

    PropertiesParser parser;

    std::string mock =  "=work";
    std::istringstream iss( mock );

    ASSERT_THROW(  parser.parse( iss ), TokenParserException );

}

TEST( properties_parser, a_token_must_always_end_with_a_equal ){

    PropertiesParser parser;

    std::string mock =  "token\n";
    std::istringstream iss( mock );

    ASSERT_THROW(  parser.parse( iss ), CharacterParserException );

}

TEST( properties_parser, a_value_can_be_empty ){

    PropertiesParser parser;

    std::string mock =  "token=\n";
    std::istringstream iss( mock );

    Properties p = parser.parse( iss );
    ASSERT_EQ( p.size(), 1 );
    ASSERT_EQ( p[ "token" ], "" );
}

TEST( properties_parser, a_value_can_contains_equals ){

    PropertiesParser parser;

    std::string mock =  "token=value=\n";
    std::istringstream iss( mock );

    Properties p = parser.parse( iss );
    ASSERT_EQ( p.size(), 1 );
    ASSERT_EQ( p[ "token" ], "value=" );
}

TEST( properties_parser, inside_a_value_all_the_special_character_are_parsed ){

    PropertiesParser parser;

    std::string mock =  "token=\\n\n";
    std::istringstream iss( mock );

    Properties p = parser.parse( iss );
    ASSERT_EQ( p.size(), 1 );
    ASSERT_EQ( p[ "token" ], "\n" );
}