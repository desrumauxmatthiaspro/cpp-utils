#include <gtest/gtest.h>

#include "generic/State.hpp"
#include "generic/StateController.hpp"

using namespace Utils::Generic;

class MockContainer {};

TEST( state_controller, on_trigger_change_state ){

    using MockStateType = State< MockContainer, char >;
    using MockStateShrPtrType = std::shared_ptr< MockStateType >;

    MockStateShrPtrType a = MockStateShrPtrType( new MockStateType() );
    MockStateShrPtrType b = MockStateShrPtrType( new MockStateType() );

    a->on( 'b', b );
    b->on( 'a', a );

    StateController< MockContainer, char > controller( a );

    ASSERT_EQ( controller.getState(), a );
    controller.execute( 'z' );
    ASSERT_EQ( controller.getState(), a );
    controller.execute( 'b' );
    ASSERT_EQ( controller.getState(), b );
    controller.execute( 'a' );
    ASSERT_EQ( controller.getState(), a );
}