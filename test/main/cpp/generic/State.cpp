#include <gtest/gtest.h>
#include "generic/State.hpp"

using namespace Utils::Generic;

class MockContainer {

public:
    int flag = 0;
};

TEST( state, on_trigger_execute_specific_handler ){

    using MockStateType = State< MockContainer, char >;
    using MockStateShrPtrType = std::shared_ptr< MockStateType >;

    MockStateShrPtrType state = MockStateShrPtrType( new MockStateType([]( MockContainer& container, char letter ) -> std::shared_ptr< MockStateType >{
        throw "bad use";
    }));

    state->on( 'a', [ &state ]( MockContainer& container, char letter ){
        container.flag++;
        return std::shared_ptr( state );
    });

    MockContainer c;
    state->execute( c, 'a' );

    ASSERT_EQ( c.flag, 1 );
}

