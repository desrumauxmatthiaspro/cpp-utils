#include <gtest/gtest.h>
#include <sstream>
#include <vector>
#include <thread>
#include <cstdlib>

#include "thread/SynchronizedPointer.hpp"

using namespace Utils::Thread;

class Mock {

    int call = 0;

public:

    int simple(){
        return call++;
    }

};

TEST( mutex_pointer, basic_usage ){

    Mock a;
    SynchronizedPointer< Mock > mock( a );

    ASSERT_EQ( mock.call( &Mock::simple ), 0 );
    ASSERT_EQ( mock.call( &Mock::simple ), 1 );

}

class ThreadMock {

    std::ostream& stream = std::cout;

public:

    ThreadMock( std::ostream& stream ) : stream( stream ){

    }

    void print( std::string str ) {
        stream << str << "\n";
        std::this_thread::sleep_for(std::chrono::milliseconds(
            std::rand() % 4 + 1
        ));
    }

};


void function(
        SynchronizedPointer< ThreadMock > mock,
        std::vector< std::string > toPrint ){

    for( std::string msg : toPrint ){
        mock.call( &ThreadMock::print, msg );
    }
}

TEST( mutex_pointer, thread_with_stream_is_secured ){

    std::stringstream ss;
    ThreadMock t( ss );
    SynchronizedPointer< ThreadMock > mock( t );

    std::vector< std::string > v = { "Test", "Work", "Great", "Bigger variable is greater" };
    std::vector< std::string > v2 = { "Bug", "Fail", "Try Again", "Must success at leat one time" };
    
    std::thread t1( function, mock, v );
    std::thread t2( function, mock, v2 );

    t1.join();
    t2.join();

    int count = 0;
    std::map< std::string, bool > checker;

    std::istringstream iss( ss.str() );
    char line[ BUFSIZ ];
    while( iss.getline( line, BUFSIZ, '\n' ) ){

        ASSERT_FALSE( checker[ line ] );
        checker[ line ] = true;
        count++;

    }

    ASSERT_EQ( count, v.size() + v2.size() );
}