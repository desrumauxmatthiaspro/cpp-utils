#include <gtest/gtest.h>
#include <sstream>

#include "logger/BasicLogger.hpp"

using namespace Utils::Logger;

TEST( basic_logger, log_with_off_level_log_nothing ){

    std::stringstream ss;
    BasicLogger logger( ss, ALL );
    logger.log( OFF, "a dummy message" );

    ASSERT_EQ( ss.str(), "" );

}

TEST( basic_logger, log_is_not_displayed_when_the_level_is_over ){

    std::stringstream ss;
    BasicLogger logger( ss, INFO, "%message" );
    
    logger.log( INFO, "TEST" );
    ASSERT_EQ( ss.str(), "TEST\n" );

    ss.str("");
    logger.log( (LogLevel)(INFO + 1), "DON'T WORK" );
    ASSERT_EQ( ss.str(), "" );

    ss.str("");
    logger.log( (LogLevel)(INFO - 1), "MUST WORK" );
    ASSERT_EQ( ss.str(), "MUST WORK\n" );
}


TEST( basic_logger, logger_level_as_value ){
    
    for( unsigned int level = OFF + 1; level <= ALL; level++ ){

        LogLevel l = (LogLevel) level;

        std::stringstream ss;
        BasicLogger logger( ss, ALL, "%level" );
        logger.log( l, "" );

        ASSERT_EQ( ss.str(), name( l ) + "\n" );

    }

}

TEST( basic_logger, logger_with_stage_for_detail ){

    std::stringstream ss;

    const std::string s1 = "TEST",
                        s2 = "CRASH",
                        s3 = "NEVER!" ;

    BasicLogger logger( ss, ALL, "%stages" );
    
    logger.log( ALL, "" );
    ASSERT_EQ( ss.str(), "@\n" );

    logger.pushStage( s1 );
    ss.str("");
    logger.log( ALL, "" );
    ASSERT_EQ( ss.str(), "@" + s1 + "\n" );

    logger.pushStage( s2 );
    ss.str("");
    logger.log( ALL, "" );
    ASSERT_EQ( ss.str(), "@" + s1 + ">" + s2 + "\n" );

    logger.pushStage( s3 );
    ss.str("");
    logger.log( ALL, "" );
    ASSERT_EQ( ss.str(), "@" + s1 + ">" + s2 + ">" + s3 + "\n" );

    logger.popStage();
    ss.str("");
    logger.log( ALL, "" );
    ASSERT_EQ( ss.str(), "@" + s1 + ">" + s2 + "\n" );

    logger.popStage();
    ss.str("");
    logger.log( ALL, "" );
    ASSERT_EQ( ss.str(), "@" + s1 + "\n" );

    logger.pushStage( s3 );
    ss.str("");
    logger.log( ALL, "" );
    ASSERT_EQ( ss.str(), "@" + s1 + ">" + s3 + "\n" );

}