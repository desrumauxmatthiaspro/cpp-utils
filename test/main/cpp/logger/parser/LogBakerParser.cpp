#include <gtest/gtest.h>
#include <sstream>

#include "logger/parser/LogBakerParser.hpp"

using namespace Utils::Logger;

TEST( log_baker_parser, on_empty_pattern_no_error ){

    LogBakerParser parser;
    std::istringstream iss( "" );
    
    std::string p = parser.parse( iss );
    ASSERT_EQ( p, "" );
}

TEST( log_baker_parser, with_no_variable ){

    LogBakerParser parser;
    std::string msg = "the parser baker test";
    std::istringstream iss( msg );
    
    std::string p = parser.parse( iss );
    ASSERT_EQ( p, msg );
}

TEST( log_baker_parser, with_a_single_variable_in_pattern ){

    LogBakerParser parser;
    std::istringstream iss( "%msg" );

    parser[ "msg" ] = "work";
    
    std::string p = parser.parse( iss );
    ASSERT_EQ( p, "work" );
}

TEST( log_baker_parser, throw_error_on_missing_variable ){

    LogBakerParser parser;
    std::istringstream iss( "%msg" );

    ASSERT_THROW( parser.parse( iss ), std::domain_error );
    
}

TEST( log_baker_parser, throw_error_on_empty_token ){

    LogBakerParser parser;
    std::istringstream iss( "% it's bad way to use my parser (-_-)" );
    
    ASSERT_THROW( parser.parse( iss ), Utils::Exception::IllegalStateException );
    
}

TEST( log_baker_parser, with_multiple_variable ){

    LogBakerParser parser;
    std::istringstream iss( "%msg it's the last test on this parser %smile" );

    parser[ "msg" ] = "work";
    parser[ "smile" ] = "(^-^)";
    
    std::string p = parser.parse( iss );
    ASSERT_EQ( p, "work it's the last test on this parser (^-^)" );
}