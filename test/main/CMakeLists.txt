# Add all *.cpp test here but always ignore test main file
set( TEST_UNIT

        cpp/logger/parser/LogBakerParser.cpp
        cpp/logger/BasicLogger.cpp

        cpp/generic/State.cpp
        cpp/generic/StateController.cpp

        cpp/parser/PropertiesParser.cpp
        
        cpp/thread/SynchronizedPointer.cpp )

#
# Setup a test unit program
#
add_executable( ${PROJECT_NAME}_runAllTest cpp/main.cpp ${TEST_UNIT} )
target_link_libraries( ${PROJECT_NAME}_runAllTest ${PROJECT_NAME} gtest gmock_main gmock )
add_test( test ${PROJECT_NAME}_runAllTest )

set_tests_properties( ${test} PROPERTIES TIMEOUT 10 )
