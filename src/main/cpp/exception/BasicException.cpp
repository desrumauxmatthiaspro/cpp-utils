#include "exception/BasicException.hpp"


/**
 * @brief Construct a new Basic Exception object
 * 
 * @param message is the explaination of this exception
 */
Utils::Exception::BasicException::BasicException( std::string message ) : message( message ){

}

/**
 * @brief Destroy the Basic Exception object
 */
Utils::Exception::BasicException::~BasicException() noexcept {}

/**
 * @brief return the situation brief. This method is inherit from exception.
 * 
 * @return const char* a brief of the exception
 */
const char* Utils::Exception::BasicException::what() const noexcept {
    return message.c_str();
}