#include "parser/PropertiesParser.hpp"

/**
 * @brief Build in a single time the parser tree
 * 
 * @return std::shared_ptr< State > the initial state
 */
std::shared_ptr<  Utils::Parser::PropertiesParser::State > Utils::Parser::PropertiesParser::buildState() const {
    using state_type = std::shared_ptr<  Utils::Parser::PropertiesParser::State >;

    // setup state
    state_type token = state_type( new State() );
    state_type value = state_type( new State() );
    state_type escapedCharacter = state_type( new State() );

    // build routine on token
    
    token->onDefault( [token]( PropertiesParserData& data, char letter ){
        data.token += letter;
        return token;
    });

    token->on( '\n', []( PropertiesParserData& data, char letter ) -> std::shared_ptr< State >{
        throw CharacterParserException( '\n', "=" );
    });

    token->on( '=', [value]( PropertiesParserData& data, char letter ){

        if( data.token == "" ){
            throw TokenParserException( "The token is empty." );
        }

        return value;
    });

    // build routine on value
    
    value->onDefault( [value]( PropertiesParserData& data, char letter ){
        data.value += letter;
        return value;
    });

    value->on( '\\', [escapedCharacter]( PropertiesParserData& data, char letter ){
        return escapedCharacter;
    });

    value->on( '\n', [token]( PropertiesParserData& data, char letter ){
        data.properties[ data.token ] = data.value;
        data.token = data.value = "";
        return token;
    });

    // escaped character routine
    
    escapedCharacter->onDefault([value]( PropertiesParserData& data, char letter ){
        
        switch( letter ){

            case 'n':
                data.value += '\n';
                break;
            
            // TODO add all escaped character here

        }

        return value;
    });

    return token;
}

/**
 * @brief finialize the value got by the parsing
 * 
 * @param partial is the parser state
 * @return constexpr ParserResultType&& is the result get by the parsing
 */
Utils::Parser::Properties  Utils::Parser::PropertiesParser::finialize( ParserState& partial ) const {

    if( partial.token != "" ){
        partial.properties[ partial.token ] = partial.value;
    }

    return partial.properties;
}