#include "parser/exception/TokenParserException.hpp"

/**
 * @brief Construct a new Token Parser Exception object
 * 
 * @param reason give a detailed why the token is invalid.
 */
Utils::Parser::TokenParserException::TokenParserException( std::string reason ){
    message = "The token is invalid. reason: " + reason;
}

/**
 * @brief Destroy the Token Parser Exception object
 */
Utils::Parser::TokenParserException::~TokenParserException() noexcept {}