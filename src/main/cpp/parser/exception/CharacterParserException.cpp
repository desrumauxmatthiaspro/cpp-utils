#include "parser/exception/CharacterParserException.hpp"

/**
 * @brief Construct a new Character Parser Exception object
 * 
 * @param current is the character during the parsing
 * @param expected is the character awaited
 */
Utils::Parser::CharacterParserException::CharacterParserException( char current, std::string expected ) {

    std::stringstream ss;
    ss << "The character given doesn't the expected one(s).\n"
            << " - current : " << current << "\n"
            << " - expected : " << expected;

    message = ss.str();
}

/**
 * @brief Destroy the Character Parser Exception object
 */
Utils::Parser::CharacterParserException::~CharacterParserException() noexcept {

}