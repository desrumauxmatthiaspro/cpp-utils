#include "logger/parser/LogBakerParser.hpp"

/**
 * @brief Build in a single time the parser tree
 * 
 * @return std::shared_ptr< State > the initial state
 */
std::shared_ptr<  Utils::Logger::LogBakerParser::State > Utils::Logger::LogBakerParser::buildState() const {
    
    using state_type_generique = std::shared_ptr<Utils::Generic::State<Utils::Logger::LogBakerParserData, char>>;
    using state_type = std::shared_ptr<  Utils::Logger::LogBakerParser::State >;

    // setup state
    state_type token = state_type( new State() );
    state_type pattern = state_type( new State() );
    state_type escapedCharacter = state_type( new State() );

    // build routine on pattern
    
    pattern->onDefault( [token, pattern]( LogBakerParserData& data, char letter ){
        
        if( letter == '%' ){

            data.logPattern.push_back( data.logPatternFragement );
            data.logPatternFragement = "";

            return token;
        }

        data.logPatternFragement += letter;
        return pattern;

    });

    pattern->on( '\\', [escapedCharacter]( LogBakerParserData& data, char letter ){
        return escapedCharacter;
    });

    // build routine on token
    
    token->onDefault( [token, pattern]( LogBakerParserData& data, char letter ) -> state_type_generique {

        if( (letter >= 'a' && letter <= 'z') ||
            (letter >= 'A' && letter <= 'Z') ){

            data.token += letter;
            return token;
        }

        if( data.token == "" ){
            throw Utils::Exception::IllegalStateException( "A token can't be empty" );
        }

        data.tokens.push_back( data.token );
        data.token = "";
        return pattern->execute( data, letter );
    });

    // escaped character routine
    
    escapedCharacter->onDefault([pattern]( LogBakerParserData& data, char letter ){
        
        switch( letter ){

            case '%':
                data.logPatternFragement += '%';
                break;

            case 'n':
                data.logPatternFragement += '\n';
                break;
            
            // TODO add all escaped character here

        }

        return pattern;
    });

    return pattern;
}

/**
 * @brief finialize the value got by the parsing
 * 
 * @param partial is the parser state
 * @return constexpr ParserResultType&& is the result get by the parsing
 */
std::string Utils::Logger::LogBakerParser::finialize( ParserState& partial ) const {

    if( partial.token != "" ){
        partial.tokens.push_back( partial.token );
    }

    partial.logPattern.push_back( partial.logPatternFragement );

    std::stringstream ss;
    ss << partial.logPattern[ 0 ];

    for( unsigned int i = 1; i < partial.logPattern.size(); i++ ){

        std::string t = partial.tokens[ i - 1 ];
        auto it = find( t );
        if( it == end() ){
            throw std::domain_error( t + " is undefined by the logger." );
        }

        ss << it->second << partial.logPattern[ i ];
    }
    
    return ss.str();
}