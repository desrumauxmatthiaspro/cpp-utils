#include "logger/BasicLogger.hpp"

/**
 * @brief Construct a new Abstract Logger object
 * 
 * @param output is where the log are written
 * @param level is the default logger level
 * @param pattern how the message of the logger must be builded
 */
Utils::Logger::BasicLogger::BasicLogger( std::ostream& output, LogLevel level, std::string pattern ) :
    pattern( pattern ), output(output) {

    setLoggerLevel( level );
}

/**
 * @brief Get the Logger Level object
 * 
 * @return LogLevel the current logger level
 */
Utils::Logger::LogLevel Utils::Logger::BasicLogger::getLoggerLevel(){
    return level;
}

/**
 * @brief Set the Logger Level object
 * 
 * @param level is the current logger level
 */
void Utils::Logger::BasicLogger::setLoggerLevel( LogLevel level ){

    std::stringstream ss;

    ss << "Logger: The level logging has been set on " << name( level ); 

    this->level = level;
    log( INTERNAL, ss.str() );
    
}

/**
 * @brief print a message on the logger
 * 
 * @param level is the level logged
 * @param message is the message to print
 */
void Utils::Logger::BasicLogger::log( LogLevel level, std::string message ){

    if( level > this->level || level <= OFF ){
        return; // SKIP IT
    }

    // create stages value
    std::stringstream stages;
    stages << "@";
    
    if( this->stages.size() >= 1 ){
        stages << this->stages[0];
    }

    for( unsigned int i = 1; i < this->stages.size(); i++ ){
        stages << ">" << this->stages[i];
    }

    // populate parser value
    parser[ "message" ] = message;
    parser[ "level" ] = name( level );
    parser[ "stages" ] = stages.str();

    std::istringstream iss( pattern );
    output << parser.parse( iss ) << "\n";
}

/**
 * @brief add a stage to the logger
 * 
 * @param stage is the stage name to add
 */
void Utils::Logger::BasicLogger::pushStage( std::string stage ){
    stages.push_back( stage );
}

/**
 * @brief remove the last stage
 */
void Utils::Logger::BasicLogger::popStage(){
    stages.pop_back();
}