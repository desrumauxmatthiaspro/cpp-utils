#pragma once

#include <mutex>
#include <memory>

namespace Utils::Thread {

/**
 * @brief Create a safe manipulator
 * 
 * @tparam Type is the class used in the safe manipulator
 */
template< class Type >
class SynchronizedPointer {

    /**
     * @brief It's the data shared between the SynchronizedPointer object
     */
    struct PointerData {

        /**
         * @brief It's the mutex to avoid insecurity
         */
        std::mutex mutex;

        /**
         * @brief It's the object stored
         */
        Type object;

    };

    /**
     * @brief it's the data shared between all the SynchronizedPointer of a same object
     */
    std::shared_ptr< PointerData > pointer;

public:

    /**
     * @brief Construct a new Synchronized Pointer object
     * 
     * @param object as initial value
     */
    SynchronizedPointer( Type object ) :
        pointer( new PointerData{ std::mutex(), object } ) {

    }

    /**
     * @brief it's a shortcut to easily write call function
     */
    #define _call( member_ptr, args, with_result ) \
        pointer->mutex.lock(); \
        with_result (pointer->object.*member_ptr)( args ); \
        pointer->mutex.unlock()

    /**
     * @brief call a member function on the object with a safe call
     * 
     * @tparam Args is the arguments type of the function
     * @tparam Result is the result given by the member function
     * @param member is a fonction of the object
     * @param args is the argument 
     * @return Result is the result of the member function
     */
    template< class Result, class... Args >
    Result call( Result ( Type::*member )( Args... args ), Args... args ) const {

        if constexpr( std::is_same<Result, void>::value ) {

            _call( member, args..., );

        } else {

            _call( member, args..., Result result = );
            return result;

        }
    }

    // remove shortcut
    #undef _call

};

}