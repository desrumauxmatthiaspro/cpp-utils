#pragma once

#include "BasicParser.hpp"
#include "exception/TokenParserException.hpp"

namespace Utils::Parser {

class Properties : public std::map< std::string, std::string > {

};

struct PropertiesParserData {

    /**
     * @brief Store all the parsed properties
     */
    Properties properties;

    /**
     * @brief It's the current token
     */
    std::string token;

    /**
     * @brief It's the value corresponding to the current token
     */
    std::string value;
};

class PropertiesParser : public BasicParser< Properties, PropertiesParserData > {

protected:

    /**
     * @brief Build in a single time the parser tree
     * 
     * @return std::shared_ptr< State > the initial state
     */
    virtual std::shared_ptr< State > buildState() const;

    /**
     * @brief finialize the value got by the parsing
     * 
     * @param partial is the parser state
     * @return constexpr ParserResultType&& is the result get by the parsing
     */
    virtual Properties finialize( ParserState& partial ) const;

};

}

// PropertiesParser.hpp