#pragma once

#include <exception>
#include <string>
#include <map>
#include <functional>
#include <memory>

#include "AbstractParser.hpp"
#include "exception/CharacterParserException.hpp"

#include "generic/State.hpp"
#include "generic/StateController.hpp"

namespace Utils::Parser {

template< class ParserResultType, class ParserStateData >
class BasicParser : public AbstractParser< 
                        ParserResultType,
                        Utils::Generic::StateController< ParserStateData, char >  > {

protected:

    using _State = Utils::Generic::State< ParserStateData, char >;
    using ParserState = Utils::Generic::StateController< ParserStateData, char >;

    class State : public _State {

    public:

        /**
         * @brief Construct a new State object with exception on unhandled character
         */
        State() : _State([ this ]( const ParserStateData& storage, const char letter ) -> std::shared_ptr< State > {
            std::string handled = "";
            auto handlers = this->getHandlers();
            for( auto it = handlers.begin(); it != handlers.end(); it++ ) {
                handled += it->first;
            }

            throw CharacterParserException( letter, handled );
        }) {}
    
    };

    /**
     * @brief Build in a single time the parser tree
     * 
     * @return std::shared_ptr< State > the initial state
     */
    virtual std::shared_ptr< State > buildState() const = 0;

    /**
     * @brief the initial state
     */
    std::shared_ptr< State > initial;

    /**
     * @brief Initialize a new instance of parser state
     * 
     * @return InternalParserStateClass&& a new parser state object
     */
    virtual ParserState initialize() const {

        // TODO Fix buildState initialization
        // here the state tree are rebuild each time we parse a new content
        return ParserState( buildState() );
    }

    /**
     * @brief process the letter get from parsing
     * 
     * @param partial is the parser state
     * @param letter is the current letter
     */
    virtual void execute( ParserState& partial, const char letter ) const {
        partial.execute( letter );
    }

};

}

// BasicParser.hpp