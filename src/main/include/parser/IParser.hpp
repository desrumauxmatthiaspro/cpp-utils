#pragma once

#include <string>

namespace Utils::Parser {

template< class ParserResultType >
class IParser {

public:

    /**
     * @brief parse from any stream a content
     * 
     * @param inputStream is the input stream where the data come from
     * @return constexpr ParserResultType&& is the result data
     */
    virtual ParserResultType parse( std::istream& inputStream ) const = 0;

    /**
     * @brief parse from a file
     * 
     * @param path is the file path to parse
     * @return constexpr ParserResultType&& is the result data
     */
    virtual ParserResultType parseFile( const std::string path ) const = 0;

};

}

// IParser.hpp