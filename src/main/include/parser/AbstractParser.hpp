#pragma once

#include <exception>
#include <string>
#include <fstream>

#include "IParser.hpp"

namespace Utils::Parser {

template< class ParserResultType, class InternalParserStateClass >
class AbstractParser : public IParser< ParserResultType > {

protected:

    /**
     * @brief Initialize a new instance of parser state
     * 
     * @return InternalParserStateClass&& a new parser state object
     */
    virtual InternalParserStateClass initialize() const = 0;

    /**
     * @brief process the letter get from parsing
     * 
     * @param partial is the parser state
     * @param letter is the current letter
     */
    virtual void execute( InternalParserStateClass& partial, const char letter ) const = 0;

    /**
     * @brief finialize the value got by the parsing
     * 
     * @param partial is the parser state
     * @return ParserResultType&& is the result get by the parsing
     */
    virtual ParserResultType finialize( InternalParserStateClass& partial ) const = 0;

public:

    /**
     * @brief parse from any stream a content
     * 
     * @param inputStream is the input stream where the data come from
     * @return ParserResultType&& is the result data
     */
    virtual ParserResultType parse( std::istream& inputStream ) const {

        InternalParserStateClass state = initialize();

        char letter;
        while( inputStream.get( letter ) ){
            execute( state, letter );
        }

        return finialize( state );
    }

    /**
     * @brief parse from a file
     * 
     * @param path is the file path to parse
     * @return constexpr ParserResultType&& is the result data
     */
    virtual ParserResultType parseFile( const std::string path ) const {

        std::filebuf buffer;
        if( ! buffer.open( path, std::ios::in ) ){
            throw std::ios_base::failure( path + " is not a readable file for some reason." );
        }

        std::istream is( &buffer );
        ParserResultType result = parse( is );
        buffer.close();

        return result;
    }

};

}

// AbstractParser.hpp