#pragma once

#include <sstream>

#include "exception/BasicException.hpp"

namespace Utils::Parser {

/**
 * @brief When a character doesn't match the expected character
 */
class CharacterParserException : public Utils::Exception::BasicException {

public:

    /**
     * @brief Construct a new Character Parser Exception object
     * 
     * @param current is the character during the parsing
     * @param expected is the character awaited
     */
    CharacterParserException( char current, std::string expected );

    /**
     * @brief Destroy the Character Parser Exception object
     */
    virtual ~CharacterParserException() noexcept;

};

}

// CharacterParserException.hpp