#pragma once

#include <sstream>

#include "exception/BasicException.hpp"

namespace Utils::Parser {

/**
 * @brief When a token doesn't respect the parser rule
 */
class TokenParserException : public Utils::Exception::BasicException {

public:

    /**
     * @brief Construct a new Token Parser Exception object
     * 
     * @param reason give a detailed why the token is invalid.
     */
    TokenParserException( std::string reason );

    /**
     * @brief Destroy the Token Parser Exception object
     */
    virtual ~TokenParserException() noexcept;

};

}

// TokenParserException.hpp