#pragma once

#include "State.hpp"

namespace Utils::Generic {

template< class StateDataContainerType, class ValueType >
class StateController : public StateDataContainerType {

    using _State = State< StateDataContainerType, ValueType >;

    /**
     * @brief It's the current state of the parser
     */
    std::shared_ptr< _State > state;

public:

    /**
     * @brief Construct a new State Controller object
     * 
     * @param initial is the initial state
     */
    StateController( std::shared_ptr< _State > initial ) : state( initial ) {

    }

    /**
     * @brief Get the State object
     * 
     * @return std::shared_ptr< _State > the current state
     */
    std::shared_ptr< _State > getState(){
        return state;
    }

    /**
     * @brief Set the State object
     * 
     * @param state is the new current state
     */
    void setState( std::shared_ptr< _State > state ){
        this->state = state;
    }

    /**
     * @brief trigger state on a value
     * 
     * @param value who can change state
     */
    void execute( const ValueType value ){
        state = state->execute( *this, value );
    }

};

}