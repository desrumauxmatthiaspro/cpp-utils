#pragma once

#include <memory>

namespace Utils::Generic {

template< class StateDataContainerType, class ValueType >
class StateController;

template< class StateDataContainerType, class ValueType >
class State : public std::enable_shared_from_this< State< StateDataContainerType, ValueType > > {

protected:

    /**
     * @brief It's the handler state
     */
    using StateHandlerType = std::function< std::shared_ptr< State >( StateDataContainerType& storageObj, const ValueType value ) >;

    /**
     * @brief it's the map who store all the handlers by character
     */
    std::map< ValueType, StateHandlerType > handlers;

    /**
     * @brief The default handler of the state
     */
    StateHandlerType defaultHandler;

public:

    /**
     * @brief Get the Handlers object
     * 
     * @return std::map< ValueType, StateHandlerType > the handlers
     */
    std::map< ValueType, StateHandlerType > getHandlers(){
        return handlers;
    }

    /**
     * @brief Construct a new State object
     */
    State() {
        defaultHandler = [this]( StateDataContainerType& storageObj, const ValueType value ){
            return this->shared_from_this();
        };
    }

    /**
     * @brief Construct a new State object
     * 
     * @param defaultHandler is the default handler
     */
    State( StateHandlerType defaultHandler ) : defaultHandler( defaultHandler ){}

    /**
     * @brief Add a handler on a value
     * 
     * @param value is the trigger
     * @param handler is the handler on the value
     */
    void on( const ValueType value, StateHandlerType handler ){

        if( handlers.find( value ) != handlers.end() ){
            throw std::invalid_argument( value + " has been already handled. You can't handle a value twice in a same state." );
        }

        handlers[ value ] = handler;
    }

    /**
     * @brief Swith to another state
     * 
     * @param value is the trigger
     * @param state is the next state
     */
    void on( const ValueType value, std::shared_ptr< State > state ){
        on( value, [ state ]( StateDataContainerType& storageObj, const ValueType value ){
            return state;
        });
    }

    /**
     * @brief Set the default handler
     * 
     * @param handler is the new default handler on unhandled value
     */
    void onDefault( StateHandlerType handler ){
        defaultHandler = handler;
    }

    /**
     * @brief execute the trigger
     * 
     * @param storageObj is a object where the data are stored
     * @param value is the trigger
     * @return std::shared_ptr< State > the next state
     */
    std::shared_ptr< State > execute( StateDataContainerType& storageObj, const ValueType value ) const {

        auto it = handlers.find( value );
        if( it != handlers.end() ){
            return it->second( storageObj, value );
        }

        return defaultHandler( storageObj, value );
    }

};

}