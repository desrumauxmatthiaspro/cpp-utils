#pragma once

#include <iostream>
#include <sstream>
#include <vector>

#include "ILogger.hpp"
#include "parser/LogBakerParser.hpp"

namespace Utils::Logger {

class BasicLogger : public ILogger {

protected:

    /**
     * @brief It's the current logger level
     */
    LogLevel level;

    /**
     * @brief Stages to identify code progress
     */
    std::vector< std::string > stages;

    /**
     * @brief It's the parser who bake the final message to the output
     */
    LogBakerParser parser;

    /**
     * @brief how the message of the logger must be builded
     */
    std::string pattern;

    /**
     * @brief it's where the logs are written
     */
    std::ostream& output;

public:

    /**
     * @brief Construct a new Abstract Logger object
     * 
     * @param output is where the log are written
     * @param level is the default logger level
     * @param pattern how the message of the logger must be builded
     */
    BasicLogger(
        std::ostream& output = std::cout,
        LogLevel level = INFO,
        std::string pattern = "[%level][%stages]: %message" );

    /**
     * @brief Get the Logger Level object
     * 
     * @return LogLevel the current logger level
     */
    virtual LogLevel getLoggerLevel();

    /**
     * @brief Set the Logger Level object
     * 
     * @param level is the current logger level
     */
    virtual void setLoggerLevel( LogLevel level );

    /**
     * @brief print a message on the logger
     * 
     * @param level is the level logged
     * @param message is the message to print
     */
    virtual void log( LogLevel level, std::string message );

    /**
     * @brief add a stage to the logger
     * 
     * @param stage is the stage name to add
     */
    virtual void pushStage( std::string stage );

    /**
     * @brief remove the last stage
     */
    virtual void popStage();

};

}