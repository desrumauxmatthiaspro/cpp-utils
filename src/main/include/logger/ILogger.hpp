#pragma once

#include <string>

#include "LogLevel.enum.hpp"

namespace Utils::Logger {

class ILogger {

public:

    /**
     * @brief Get the Logger Level object
     * 
     * @return LogLevel the current logger level
     */
    virtual LogLevel getLoggerLevel() = 0;

    /**
     * @brief Set the Logger Level object
     * 
     * @param level is the current logger level
     */
    virtual void setLoggerLevel( LogLevel level ) = 0;

    /**
     * @brief print a message on the logger
     * 
     * @param level is the level logged
     * @param message is the message to print
     */
    virtual void log( LogLevel level, std::string message ) = 0;

    /**
     * @brief add a stage to the logger
     * 
     * @param stage is the stage name to add
     */
    virtual void pushStage( std::string stage ) = 0;

    /**
     * @brief remove the last stage
     */
    virtual void popStage() = 0;

};

}