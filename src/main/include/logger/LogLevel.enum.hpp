#pragma once

#include <stdexcept>

namespace Utils::Logger {

/**
 * @brief It's all the logger level.
 */
enum LogLevel {
    OFF,
    FATAL,
    INFO,
    ERROR,
    WARNING,
    DEBUG,
    ALL,
    INTERNAL // LOGGER DEBUG
};

/**
 * @brief It's a shortcut for name.
 * This macro disappear after this header.
 */
#define _name( level ) \
    case level: \
        return #level

/**
 * @brief convert a logger level into a string
 * 
 * @param level is the logger level
 * @return const std::string is the logger level name
 */
inline const std::string name( LogLevel level ){

    switch( level ){

        _name( OFF );
        _name( FATAL );
        _name( INFO );
        _name( ERROR );
        _name( WARNING );
        _name( DEBUG );
        _name( ALL );
        _name( INTERNAL );

    };

    throw std::invalid_argument( "This logger level doesn't have name." );
}

// remove the shortcut
#undef _name

}