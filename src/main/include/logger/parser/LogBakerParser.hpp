#pragma once

#include <string>
#include <vector>
#include <map>

#include "parser/BasicParser.hpp"
#include "exception/IllegalStateException.hpp"

namespace Utils::Logger {

struct LogBakerParserData {

    /**
     * @brief It's all the token who should be replace by a value
     */
    std::vector< std::string > tokens;

    /**
     * @brief It's all pattern where between two string you have a variable
     */
    std::vector< std::string > logPattern;

    /**
     * @brief It's a fragement of the log pattern
     */
    std::string logPatternFragement = "";

    /**
     * @brief It's a futur token
     */
    std::string token = "";

};

class LogBakerParser : public Utils::Parser::BasicParser< std::string, LogBakerParserData >,
                        public std::map< std::string, std::string > {

protected:

    /**
     * @brief Build in a single time the parser tree
     * 
     * @return std::shared_ptr< State > the initial state
     */
    virtual std::shared_ptr< State > buildState() const;

    /**
     * @brief finialize the value got by the parsing
     * 
     * @param partial is the parser state
     * @return constexpr ParserResultType&& is the result get by the parsing
     */
    virtual std::string finialize( ParserState& partial ) const;

};

}