#pragma once

#include <string>
#include <exception>

namespace Utils::Exception
{
    
class BasicException : public std::exception {

protected:

    /**
     * @brief It's the message to explain the exception
     */
    std::string message;

public:

    /**
     * @brief Construct a new Basic Exception object
     * 
     * @param message is the explaination of this exception
     */
    BasicException( std::string message = "" );

    /**
     * @brief Destroy the Basic Exception object
     */
    virtual ~BasicException() noexcept;

    /**
     * @brief return the situation brief. This method is inherit from exception.
     * 
     * @return const char* a brief of the exception
     */
    virtual const char* what() const noexcept;

};

} // namespace Utils::Exception