#pragma once

#include "BasicException.hpp"

namespace Utils::Exception
{
    
class IllegalStateException : public BasicException {

public:

    /**
     * @brief Construct a new Illegal State Exception object
     * 
     * @param msg is the exception message
     */
    IllegalStateException( std::string msg ) : BasicException(msg) {}

};

} // namespace Utils::Exception